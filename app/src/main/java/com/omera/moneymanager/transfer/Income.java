package com.omera.moneymanager.transfer;

import java.util.Date;

public class Income extends Transfer {

    public Income(double amount, Date date, String category, String notes) {
        super(amount, date, category, notes);

    }
}
