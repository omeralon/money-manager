package com.omera.moneymanager.transfer;

import java.util.Date;

public abstract class Transfer {
    private double amount;
    private Date date;
    private String category;
    private String notes;

    public Transfer(double amount, Date date, String category, String notes) {
        this.amount = amount;
        this.date = date;
        this.category = category;
        this.notes = notes;
    }

    public double getAmount() {
        return this.amount;
    }

    public Date getDate() {
        return this.date;
    }

    public String getCategory() {
        return this.category;
    }

    public String getNotes() {
        return this.notes;
    }
}
