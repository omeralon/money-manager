package com.omera.moneymanager.transfer;


import java.util.Date;

public class Expense extends Transfer {

    public Expense(double amount, Date date, String category, String notes) {
        super(amount, date, category, notes);

    }
}
