package com.omera.moneymanager.exceptions;

public class ValueNotFoundException extends Exception {

    public ValueNotFoundException() {

    }

    public ValueNotFoundException(String message) {
        super(message);
    }

    public ValueNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
