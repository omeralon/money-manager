package com.omera.moneymanager.DBHandler;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.omera.moneymanager.exceptions.ValueNotFoundException;
import com.omera.moneymanager.transfer.Expense;
import com.omera.moneymanager.transfer.Transfer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ExpenseTransferDBHandler extends SQLiteOpenHelper implements TransferDBHandler {

    //information of database
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "TransfersDB.db";
    public static final String TABLE_NAME = "Expenses";
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_AMOUNT = "AMOUNT";
    public static final String COLUMN_DATE = "DATE";
    public static final String COLUMN_CATEGORY = "CATEGORY";
    public static final String COLUMN_NOTES = "NOTES";

    public ExpenseTransferDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory,
                                    int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_AMOUNT + " REAL, " +
                COLUMN_DATE + " TEXT, " +
                COLUMN_CATEGORY + " TEXT, " +
                COLUMN_NOTES + " TEXT )";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void addTransfer(Transfer transfer) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_AMOUNT, transfer.getAmount());
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new
                SimpleDateFormat("yyyy-MM-dd");
        String dateToSave = sdf.format(transfer.getDate());
        values.put(COLUMN_DATE, dateToSave);
        values.put(COLUMN_CATEGORY, transfer.getCategory());
        values.put(COLUMN_NOTES, transfer.getNotes());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    @Override
    public Transfer getTransfer(int id) throws ValueNotFoundException {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + COLUMN_AMOUNT + "," + COLUMN_DATE + "," + COLUMN_CATEGORY + ","
                + COLUMN_NOTES + " FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + " =?";
        Cursor cursor = db.rawQuery(query, new String[] {String.valueOf(id)});

        if (cursor!=null) {
            cursor.moveToFirst();
        }

        try {
            double amount = Double.parseDouble(cursor.getString(0));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = sdf.parse(cursor.getString(1));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String category = cursor.getString(2);
            String notes = cursor.getString(3);
            Transfer transfer = new Expense(amount, date, category, notes);

            cursor.close();

            return transfer;
        }

        catch (IndexOutOfBoundsException e) {
            throw new ValueNotFoundException();
        }

    }

    @Override
    public void editTransfer(int id, Transfer transfer) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_AMOUNT, transfer.getAmount());
        values.put(COLUMN_DATE, transfer.getDate().toString());
        values.put(COLUMN_CATEGORY, transfer.getCategory());
        values.put(COLUMN_NOTES, transfer.getNotes());

        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_NAME, values, COLUMN_ID + "=?", new String[]
                {String.valueOf(id)});
        db.close();
    }

    @Override
    public void removeTransfer(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + "=?", new String[] {String.valueOf(id)});
        db.close();
    }

    @Override
    public List getMonthData(int monthNumber) {
        SQLiteDatabase db = this.getReadableDatabase();

        int thisYear = 2019;
        String monthFormatted = this.reformatMonthToQuery(monthNumber);
        String dateToSearchFormatted = thisYear + "-" + monthFormatted + "%";

        String query = "SELECT " + COLUMN_AMOUNT + ", " + COLUMN_DATE + ", " + COLUMN_CATEGORY +
                ", " + COLUMN_NOTES +
                " FROM " + TABLE_NAME + " WHERE " + COLUMN_DATE + " LIKE '" + dateToSearchFormatted + "'";
        Cursor cursor = db.rawQuery(query, null);

        List<Transfer> result = new ArrayList<>();

        try {
            while (cursor.moveToNext()) {
                double amount = Double.parseDouble(cursor.getString(0));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                try {
                    String d = cursor.getString(1);
                    date = sdf.parse(d);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String category = cursor.getString(0);
                String notes = cursor.getString(0);
                result.add(new Expense(amount, date, category, notes));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        finally {
            cursor.close();
        }

        return result;

    }

    public String reformatMonthToQuery(int monthNumber) {
        String result;
        if (String.valueOf(monthNumber).length() == 1) {
            result = "0" + String.valueOf(monthNumber);
        }
        else {
            result = String.valueOf(monthNumber);
        }
        return result;
    }
}
