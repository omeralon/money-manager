package com.omera.moneymanager.DBHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ExpenseCategoriesDBHandler extends SQLiteOpenHelper implements CategoriesDBHandler {
    //information of database
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "CategoriesDB.db";
    public static final String TABLE_NAME = "Expenses";
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_NAME = "NAME";

    public ExpenseCategoriesDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory,
                                    int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_NAME + " TEXT )";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void addCategory(String name) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, name);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    @Override
    public List getAllCategories() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + COLUMN_NAME + " FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);

        List<String> categories = new ArrayList<>();
        try {
            while (cursor.moveToNext()) {
                categories.add(cursor.getString(1));
            }

        } finally {
            cursor.close();
        }

        return categories;
    }

    @Override
    public void removeCategory(int id) {

    }

    @Override
    public void removeCategory(String name) {

    }
}
