package com.omera.moneymanager.DBHandler;

import java.util.List;

public interface CategoriesDBHandler {
    public void addCategory(String name);
    public List getAllCategories();
    public void removeCategory(int id);
    public void removeCategory(String name);
}
