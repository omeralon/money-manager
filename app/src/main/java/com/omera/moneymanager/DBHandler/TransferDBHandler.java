package com.omera.moneymanager.DBHandler;

import com.omera.moneymanager.exceptions.ValueNotFoundException;
import com.omera.moneymanager.transfer.Transfer;

import java.util.Date;
import java.util.List;

public interface TransferDBHandler {
    public void addTransfer(Transfer transfer);
    public Transfer getTransfer(int id) throws ValueNotFoundException;
    public void editTransfer(int id, Transfer transfer);
    public void removeTransfer(int id);
    public List getMonthData(int monthNumber);
}
