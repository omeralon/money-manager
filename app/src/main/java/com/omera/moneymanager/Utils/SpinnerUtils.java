package com.omera.moneymanager.Utils;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class SpinnerUtils{

    private Context context;
    private Spinner spinner;

    public SpinnerUtils(Context context, Spinner spinner) {
        this.spinner = spinner;
        this.context = context;
    }

    public void setSpinnerValues(String[] values) {
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.context, android.R.layout.simple_spinner_dropdown_item, values);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinner.setAdapter(arrayAdapter);
    }

}
