package com.omera.moneymanager.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

    private final String DATE_SHOW_FORMAT = "dd/MM/yyyy";
    private final String DATE_DB_FORMAT = "yyyy-MM-dd";

    public String toShow(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(this.DATE_SHOW_FORMAT);
        return sdf.format(date);
    }

    public String toDB(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(this.DATE_DB_FORMAT);
        return sdf.format(date);
    }

    public Date stringToDate(String dateAsText, String format) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.parse(dateAsText);
    }

    public String getShowFormat() {
        return this.DATE_SHOW_FORMAT;
    }
}
