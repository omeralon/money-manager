package com.omera.moneymanager.Utils;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@SuppressLint("ValidFragment")
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private final String DATESHOWFORMAT = "dd/MM/yyyy";
    private final TextView label;

    @SuppressLint("ValidFragment")
    public DatePickerFragment(TextView label) {
        this.label = label;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String userDatePick = dayOfMonth + "/" + month + "/" + year;
        updateLabel(userDatePick);
    }

    public void updateLabel(String userDatePick) {
        try {
            DateFormatter dateFormatter = new DateFormatter();
            Date date = dateFormatter.stringToDate(userDatePick, "dd/M/yyyy");
            label.setText(dateFormatter.toShow(date));
        } catch (ParseException e) {
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            label.setText("N/A");
        }
    }
}
