package com.omera.moneymanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.omera.moneymanager.DBHandler.ExpenseTransferDBHandler;
import com.omera.moneymanager.transfer.Transfer;

import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ExpenseTransferDBHandler expenseDB;
    private TextView monthExpenses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.expenseDB = new ExpenseTransferDBHandler
                (this, null, null, 1);

        monthExpenses = (TextView) findViewById(R.id.expenseValueTextView);

        int thisMonthNumber = Calendar.getInstance().MONTH + 1;
        this.monthExpenses.setText(String.format("%s ILS", Double.toString(getMonthExpenseSum(thisMonthNumber))));
    }

    @Override
    protected void onResume() {
        super.onResume();
        int thisMonthNumber = Calendar.getInstance().MONTH + 1;
        this.monthExpenses.setText(String.format("%s ILS", Double.toString(getMonthExpenseSum(thisMonthNumber))));
    }

    public void sendMessage(View view) {
        Intent intent = new Intent(this, AddTransferActivity.class);
        startActivity(intent);
    }

    public double getMonthExpenseSum(int monthNumber) {
        List<Transfer> result = this.expenseDB.getMonthData(monthNumber);
        double sum = 0;
        for (Transfer transfer : result) {
            sum = sum + transfer.getAmount();
        }
        return sum;
    }
}
