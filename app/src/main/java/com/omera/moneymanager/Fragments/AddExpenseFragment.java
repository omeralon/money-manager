package com.omera.moneymanager.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.omera.moneymanager.DBHandler.ExpenseTransferDBHandler;
import com.omera.moneymanager.DBHandler.TransferDBHandler;
import com.omera.moneymanager.R;
import com.omera.moneymanager.Utils.DateFormatter;
import com.omera.moneymanager.Utils.DatePickerFragment;
import com.omera.moneymanager.Utils.SpinnerUtils;
import com.omera.moneymanager.transfer.Expense;
import com.omera.moneymanager.transfer.Transfer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddExpenseFragment extends Fragment {

    EditText amount;
    TextView dateTextView;
    Spinner categories;
    EditText notes;
    Button addButton;
    TransferDBHandler expenseDB;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_expense, container, false);
        initializeFields(view);
        this.expenseDB = new ExpenseTransferDBHandler
                (getContext(), null, null, 1);

        dateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment datePickerFragment = new DatePickerFragment(dateTextView);
                datePickerFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addExpenseToDB();
                } catch (ParseException e) {
                    Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
                }
            }
        });

        return view;
    }

    public void initializeFields(View view) {
        this.amount = (EditText) view.findViewById(R.id.amountEditText);

        this.dateTextView = (TextView) view.findViewById(R.id.dateTextView);
        setTodayDateToDateField();

        this.categories = (Spinner) view.findViewById(R.id.categoriesSpinner);
        SpinnerUtils spinnerUtils = new SpinnerUtils(getContext(), this.categories);
        spinnerUtils.setSpinnerValues(new String[] {"a", "b", "c"});

        this.notes = (EditText) view.findViewById(R.id.notesEditText);

        this.addButton = (Button) view.findViewById(R.id.addExpenseButton);
    }

    public void setTodayDateToDateField() {
        Date now = Calendar.getInstance().getTime();
        DateFormatter dateFormatter = new DateFormatter();
        String date = dateFormatter.toShow(now);
        dateTextView.setText(date);
    }

    public void addExpenseToDB() throws ParseException {
        DateFormatter dateFormatter = new DateFormatter();
        double amount = Double.parseDouble(this.amount.getText().toString());
        Date date = dateFormatter.stringToDate(this.dateTextView.getText().toString(),
                dateFormatter.getShowFormat());
        String category = this.categories.getTransitionName();
        String notes = this.notes.toString();
        Transfer transfer = new Expense(amount, date, category, notes);
        this.expenseDB.addTransfer(transfer);
        clearFields();
        Toast.makeText(getContext(), "Added", Toast.LENGTH_LONG).show();
    }

    public void clearFields() {
        this.amount.setText("");
        setTodayDateToDateField();
//        this.categories
        this.notes.setText("");
    }

}
