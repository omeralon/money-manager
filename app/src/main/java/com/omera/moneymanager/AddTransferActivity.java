package com.omera.moneymanager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.omera.moneymanager.Fragments.AddExpenseFragment;
import com.omera.moneymanager.Fragments.AddIncomeFramgment;

public class AddTransferActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transfer);

        loadFragment(new AddExpenseFragment());

        Button expenseButton = (Button) findViewById(R.id.expenseButton);
        Button incomeButton = (Button) findViewById(R.id.incomeButton);

        expenseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new AddExpenseFragment());
            }
        });

        incomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new AddIncomeFramgment());
            }
        });
    }

    protected void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.addTransferFragment, fragment);
        fragmentTransaction.commit();
    }

}
